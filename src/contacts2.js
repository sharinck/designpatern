/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.Contacts2 = function () {

        var _contacts;

        this.add = function (contact) {
            _contacts[contact.id()] = contact;
        };

        this.get = function (id) {
            return _contacts[id];
        };

        this.change = function(strategy) {
            strategy.update(_contacts);
        };

        this.size = function () {
            var size = 0, key;
            for (key in _contacts) {
                if (_contacts.hasOwnProperty(key)) size++;
            }
            return size;
        };

        this.remove = function (id) {
            delete _contacts[id];
        };

        this.getFromName = function (firstname, lastname) {
            var key, array = [];
            for (key in _contacts) {
                if (_contacts.hasOwnProperty(key)) {
                    if(_contacts[key].firstName() === firstname &&
                        _contacts[key].lastName() ===  lastname) {
                        array.push(_contacts[key]);
                    }
                }
            }
            if(array.length === 0) {return null;}
            return array;
        };

        var init = function () {
            _contacts = [];
        };

        this.clear =  function () {
            init();
        };

        this.search = function (strategy) {
            return strategy.search( _contacts );
        };

        init();
    };

    return self;
}(Contact || {}));