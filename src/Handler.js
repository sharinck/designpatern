/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Handler = function (contact, handler) {

        var _contacts;
        var _handler;
        var _tag = null;

        this.getContact = function () {
            return _contacts;
        };

        this.getNextHandler = function () {
            return _handler;
        };

        this.process = function(contact) {
            if ( _tag) {
                if (contact.tag() === _tag) {
                    _contacts.add(contact);
                } else {
                    return false;
                }
            } else {
                _tag = contact.tag();
                _contacts.add(contact);
            }
            return true;
        };


        var init = function (contact, handler) {
            _contacts = contact;
            _handler = handler;
        };

        init(contact, handler);
    };

    return self;
}(Contact || {}));