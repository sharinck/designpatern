/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Model = function () {

        var _ic;

        var init = function(){
            _ic = Contact.Contacts.instance();
        };

        this.getContactIterator = function () {
            return _ic.iterator();
        };

        this.getContactInstance = function () {
            return _ic;
        };

        init();
    };

    return self;
}(Contact || {}));