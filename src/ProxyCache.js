/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.ProxyCache = function (list) {

        var _listContacts;
        var _cache;

        this.search = function (strategy) {
            var i, result;
            for (i = 0; i < _listContacts.length; i++){
                result = _listContacts[i].search(strategy);
                if (result){
                    this.addInCache(strategy, result);
                    return result;
                }
            }
            return null;
        };

        this.inCache = function (strategy) {
            var _cacheName = strategy.getStrategyName() + "_" + strategy.getSearchTerm();
            if (_cache[_cacheName]) {
                return true;
            }
            return false;
        };

        this.update = function (contact, key){
            delete _cache[key];
            contact.setCacheObject(null, null);
            console.log("Cache mis à jour");
        };

        this.addInCache = function (strategy, result) {
            var _cacheName = strategy.getStrategyName() + "_" + strategy.getSearchTerm();
            _cache[_cacheName] = result;
            result.setCacheObject(this, _cacheName);
        };

        var init = function () {
            _listContacts = list;
            _cache = [];
        };
        init(list);
    };

    return self;
}(Contact || {}));