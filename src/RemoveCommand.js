/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.RemoveCommand = function () {

        var _contact;

        this.execute = function (id) {
            _contact = Contact.Contacts.instance().get(id);
            Contact.Contacts.instance().remove(id);
        };

        this.undo = function () {
            Contact.Contacts.instance().add(_contact);
        };
    };

    return self;
}(Contact || {}));