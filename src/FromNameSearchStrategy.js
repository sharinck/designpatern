/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.FromNameSearchStrategy = function (firstname, lastname) {

        var _firstname, _lastname;

        this.search = function (_contacts) {
            var key;
            for (key in _contacts) {
                if (_contacts.hasOwnProperty(key)) {
                    if(_contacts[key].firstName() === _firstname &&
                        _contacts[key].lastName() ===  _lastname) {
                        return _contacts[key];
                    }
                }
            }
            return null;
        };

        this.getStrategyName = function () {
            return "FromName";
        };

        this.getSearchTerm = function (){
            return _lastname + "%" + _firtname;
        };

        var init = function (firstname, lastname) {
            _firstname = firstname;
            _lastname = lastname;
        };

        init(firstname, lastname);
    };

    return self;
}(Contact || {}));