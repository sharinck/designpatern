/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Iterator = function (contacts) {

        var _contacts;
        var _currentIndex;
        var _keys;

        var init = function () {
            _contacts = contacts;
            _currentIndex = 0;
            _keys = Object.keys(_contacts.getAll());
        };

        this.hasNext = function () {
            if(_currentIndex < _keys.length) return true;
            return false;
        };

        this.next = function () {
            return _contacts.get(_keys[_currentIndex++]);
        };

        this.first = function () {
            return _contacts.get(_keys[0]);
        };

        this.each = function (fx) {
            //blablabla
            while(this.hasNext()){
                var x = this.next();
                fx(x);
            }
        };


        init(contacts);
    };

    return self;
}(Contact || {}));