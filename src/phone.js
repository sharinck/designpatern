/**
 * Created by TonySma on 10/09/2015.
 */
/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.PhoneCategory = { PERSO: 0, PRO: 1 };
    self.PhoneType = { MOBILE: 0, FIXE: 1, PHONE: 1 };
    self.Phone = function (number, category, type) {

        var _number;
        var _category;
        var _type;

        this.number = function () {
            return _number;
        };

        this.category = function () {
            return _category;
        };

        this.type = function () {
            return _type;
        };

        this.setNumber = function (number) {
            _number = number;
        };

        var init = function (number, category, type) {
            _number = number;
            _category = category;
            _type = type;
        };

        init(number, category, type);
    };

    return self;
}(Contact || {}));