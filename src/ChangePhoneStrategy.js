/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.ChangePhoneStrategy = function (firstnane, lastname, currentNum, newNum) {

        var _firstname, _lastname, _currentNum, _newNum;

        var findPhone = function(_phones) {
            for (var i = 0;  i < _phones.length; i++  ){
                if(_phones[i].number() == _currentNum) return _phones[i];
            }
            return null;
        };

        this.update = function(_contacts) {
            var contactToModify = new Contact.FromPhoneSearchStrategy(currentNum).search(_contacts);
            if (contactToModify) {
                var  p = findPhone(contactToModify.phones());
                if (p) {
                    p.setNumber(_newNum);
                    contactToModify.isModified();
                    return true;
                }
            }
            return false;
        };

        var init = function (firstname, lastname, currentNum, newNum) {
            _firstname = firstname;
            _lastname = lastname;
            _currentNum = currentNum;
            _newNum = newNum;
        };

        init(firstnane, lastname, currentNum, newNum);
    };

    return self;
}(Contact || {}));