/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    var contactsInstance = null;
    var Contacts = function () {

        var _contacts;

        this.add = function (contact) {
            _contacts[contact.id()] = contact;
        };

        this.get = function (id) {
            return _contacts[id];
        };

        this.getAll = function () {
            return _contacts;
        };

        this.size = function () {
            var size = 0, key;
            for (key in _contacts) {
                if (_contacts.hasOwnProperty(key)) size++;
            }
            return size;
        };

        this.remove = function (id) {
            delete _contacts[id];
        };

        this.getFromName = function (firstname, lastname) {
            var key, array = [];
            for (key in _contacts) {
                if (_contacts.hasOwnProperty(key)) {
                    if(_contacts[key].firstName() === firstname &&
                        _contacts[key].lastName() ===  lastname) {
                        array.push(_contacts[key]);
                    }
                }
            }
            if(array.length === 0) {return null;}
            return array;
        };

        var init = function () {
            _contacts = {};
        };

        this.clear =  function () {
            init();
        };

        this.search = function (strategy) {
            return strategy.search( _contacts );
        };

        this.load = function(Icontacts){

        };

        this.iterator = function () {
            return new Contact.Iterator(this);
        };

        init();
    };

    self.Contacts = {
        instance: function () {
            if (!contactsInstance) {
                contactsInstance = new Contacts();
            }
            return contactsInstance;
        }
    };

    return self;
}(Contact || {}));