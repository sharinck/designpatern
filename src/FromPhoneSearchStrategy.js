/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.FromPhoneSearchStrategy = function (phone) {

        var _phone;

        this.search = function (_contacts) {
            var key;
            for (key in _contacts) {
                if (_contacts.hasOwnProperty(key)) {
                    var phonesList = _contacts[key].phones();
                    if (phoneExist(phonesList)) {
                        return _contacts[key];
                    }
                }
            }
            return null;
        };

        this.getStrategyName = function () {
            return "FromPhone";
        };

        this.getSearchTerm = function (){
            return _phone;
        };

        var phoneExist = function (phonesList) {
            var i;
            for (i = 0; i < phonesList.length; i++) {
                if (phonesList[i].number() === _phone) {
                    return true;
                }
            }
            return false;
        };

        var init = function (phone) {
            _phone = phone;
        };

        init(phone);
    };

    return self;
}(Contact || {}));