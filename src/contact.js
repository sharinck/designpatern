/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Gender = {MR: 1, MME: 2};
    self.Contact = function (gender, firstname, lastname) {


        var _id;
        var _gender;
        var _firstname;
        var _lastname;
        var _tag = null;

        var _cacheObject;
        var _cacheKey;

        var _mails = [];
        var _phones = [];

        this.id = function () {
            return _id;
        };

        this.gender = function () {
            return _gender;
        };

        this.firstName = function () {
            return _firstname;
        };

        this.lastName = function () {
            return _lastname;
        };

        this.mails = function () {
            return _mails;
        };

        this.phones = function () {
            return _phones;
        };

        this.tag = function () {
            return _tag;
        };

        this.addMail = function (mail) {
            _mails.push(mail);
        };

        this.addPhone = function (phone) {
            _phones.push(phone);
        };

        this.setTag = function (tag){
            _tag = tag;
        };

        this.addTag = function (tag){
            _tag = tag;
        };

        this.setCacheObject = function (obj, key) {
            _cacheObject = obj;
            _cacheKey = key;
        };

        this.isModified = function () {
            _cacheObject.update(this , _cacheKey);
        };

        var generateId = function () {
            var id = "xxxx-xxxx-xxxx-xxxx-xxxx".replace(/x/g, function (c) {
                var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r&0x3 | 0x8);
                return v.toString(16);
            });
            return id;
        };

        this.getJSON = function () {
            var out = "{";
            out += "\"id\": "  + this.id();
            out += ", ";
            out += "\"gender\": "  + this.gender();
            out += ", ";
            out += "\"firstname\": "  + this.firstName();
            out += ", ";
            out += "\"lastname\": "  + this.lastName();
            out += ", ";
            out += "\"tag\": "  + this.tag();
            out += ", ";

            out += "\"mails\":[";
            var mails = this.mails();
            for (var i = 0; i < mails.length ; i++)  {
                out += "{\"mail\":[ ";
                out += "\"address\" : " + mails[i].address();
                out += ", ";
                out += "\"category\" : " + mails[i].category();
                out += ", ";
                out += "]}";
            }
            out += "] ";

            out += "\"phones\":[";
            var phones = this.phones();
            for (var i = 0; i < phones.length ; i++)  {
                out += "{\"phone\":[ ";
                out += "\"number\" : " + phones[i].number();
                out += ", ";
                out += "\"type\" : " + phones[i].type();
                out += ", ";
                out += "\"category\" : " + phones[i].category();
                out += ", ";
                out += "]}";
            }
            out += "] ";

            out += "}";

            return out;

        };

        var init = function (gender, firstname, lastname) {
            _id = generateId();
            _gender = gender;
            _firstname = firstname;
            _lastname = lastname;
            _cacheObject = null;
            _cacheKey = null;
        };

        init(gender, firstname, lastname);
    };

    return self;
}(Contact || {}));