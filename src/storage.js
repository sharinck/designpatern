/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    var storageInstance = null;
    var Storage = function () {

        var saveInLocal = function (contact) {
            //console.log(contact.getJSON());
            sessionStorage.setItem("contact/" + contact.id(), contact.getJSON() );
        };

        this.save = function ( contacts) {
            var key;
            var list = contacts.getAll();
            for (key in list) {
                if (list.hasOwnProperty(key)){
                    saveInLocal(list[key]);
                }
            }
        };

        this.iterator = function () {};

        var init = function () {};

        init();
    };

    self.Storage = {
        instance: function () {
            if (!storageInstance) {
                storageInstance = new Storage();
            }
            return storageInstance;
        }
    };

    return self;
}(Contact || {}));