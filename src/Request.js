/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Request = function (request) {

        var _request;

        this.getRequest = function () {
            return _request;
        };

        var init = function (request) {
            _request = request;
        };

        init(request);
    };

    return self;
}(Contact || {}));