/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Controller = function (model, view) {

        var _model, _view;

        var init = function(model, view){
            _model = model;
            _view = view;

            $('html').on("click", "button", function () {
                var id = $(this).attr("id");
                id = id.substring(7);
                model.getContactInstance().remove(id);

                model = new Contact.Model();
                view = new Contact.View(model);
            });
        };

        init(model, view);
    };

    return self;
}(Contact || {}));