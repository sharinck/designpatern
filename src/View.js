/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.View = function (model) {

        var _model;

        var generatePhonesHtml = function (_phones) {
            var html = "", key, bool=true;
            for (key in _phones) {
                if (_phones.hasOwnProperty(key)){
                    if(!bool) html+= "/";
                    if(bool) bool=false;

                    html += _phones[key].number();

                    if(_phones[key].category() == 1){
                        html += "[PRO]";
                    }
                    else{
                        html += "[PERSO]";
                    }

                    if(_phones[key].type() == 1){
                        html += "[PHONE]";
                    }
                    else{
                        html += "[MOBILE]";
                    }
                }
            }
            return html;
        };

        var generateMailHtml = function (_mails) {
            var html = "", key, bool=true;
            for (key in _mails) {
                if (_mails.hasOwnProperty(key)){
                    if(!bool) html+= "/";
                    if(bool) bool=false;


                    html += _mails[key].address();
                    if(_mails[key].category() == 2){
                        html += "[PRO]";
                    }
                    else{
                        html += "[PERSO]";
                    }
                }
            }
            return html;
        };

        var buildUI = function(){
            $("#contacts").html("");

            var html = $("<table/>");
            var line = $("<tr/>").appendTo(html);

            /* Table Header */
            $("<th/>", {id:'cellFirstName', html: 'First name'}).appendTo(line);
            $("<th/>", {id:'cellLastName', html: 'Last name'}).appendTo(line);
            $("<th/>", {id:'cellPhones', html: 'Phones'}).appendTo(line);
            $("<th/>", {id:'cellMails', html: 'Mails'}).appendTo(line);
            $("<th/>", {id:'cellTags', html: 'Tags'}).appendTo(line);
            $("<th/>", {id:'cellActions',html: 'Actions'}).appendTo(line);

            /*Table Rows */
            var contacts = model.getContactIterator();
            while (contacts.hasNext()) {
                var contact = contacts.next();
                if (contact) {
                    var line = $("<tr/>",{ id:'x' + contact.id() }).appendTo(html);
                    $("<td/>", {id:'cellFirstName', html: contact.firstName()}).appendTo(line);
                    $("<td/>", {id:'cellLastName', html: contact.lastName()}).appendTo(line);
                    $("<td/>", {id:'cellPhones', html: generatePhonesHtml(contact.phones())}).appendTo(line);
                    $("<td/>", {id:'cellMails', html: generateMailHtml(contact.mails())}).appendTo(line);
                    $("<td/>", {id:'cellTags', html: contact.tag()}).appendTo(line);
                    $("<td/>", {id:'cellActions',html:
                        $("<button/>",{html:"Supprimer", class:'btn btn-danger', id:'button_'+contact.id() } )}
                    ).appendTo(line);

                }
            }

            html.appendTo("#contacts");
        };

        var init = function(model){
            _model = model;
            buildUI();
        };

        init(model);
    };

    return self;
}(Contact || {}));