/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.AddCommand = function () {

        var _contact;

        this.execute = function (contact) {
            _contact = contact;
            Contact.Contacts.instance().add(_contact);
        };

        this.undo = function () {
            var id = _contact.id();
            Contact.Contacts.instance().remove(id);
        };
    };

    return self;
}(Contact || {}));