/**
 * Created by TonySma on 11/09/2015.
 */
/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}

Contact = (function (self) {
    'use strict';

    self.FromMailSearchStrategy = function (mail) {

        var _mail;

        this.getStrategyName = function () {
            return "FromMail";
        };

        this.getSearchTerm = function (){
            return _mail;
        };

        this.search = function (_contacts) {
            var key;
            for (key in _contacts) {
                if (_contacts.hasOwnProperty(key)) {
                    var mailList = _contacts[key].mails();
                    if (mailExist(mailList)) {
                        return _contacts[key];
                    }
                }
            }
            return null;
        };

        var mailExist = function (mailList) {
            var i;
            for (i = 0; i < mailList.length; i++) {
                if (mailList[i].address() === _mail) {
                    return true;
                }
            }
            return false;
        };

        var init = function (mail) {
            _mail = mail;
        };

        init(mail);
    };

    return self;
}(Contact || {}));