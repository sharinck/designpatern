/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.MailCategory = { PERSO: 1, PRO: 2 };
    self.Mail = function (address, category) {

        var _address;
        var _category;

        this.address = function () {
            return _address;
        };

        this.category = function () {
            return _category;
        };

        var init = function (address, category) {
            _address = address;
            _category = category;
        };

        init(address, category);
    };

    return self;
}(Contact || {}));