/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Chain = function (handler) {

        var _handler;

        this.getHandler = function () {
            return _handler;
        };

        this.processRequest = function (request) {
            var localHandler = _handler;
            while (!localHandler.process(request.getRequest())) {
                localHandler = localHandler.getNextHandler();
            }
        };

        var init = function (handler) {
            _handler = handler;
        };

        init(handler);
    };

    return self;
}(Contact || {}));