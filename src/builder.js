/**
 * Created by TonySma on 10/09/2015.
 */
var Contact = Contact || {}


Contact = (function (self) {
    'use strict';

    self.Builder = function () {

        this.createMinimalContact = function (gender, firstname, lastname) {
            var contact = new Contact.Contact(gender, firstname, lastname);
            return contact;
        };

        this.createContactWithProfessionalMail = function (gender, firstname, lastname, mail){
            var _contact = new Contact.Contact(gender, firstname, lastname);
            var _mail = new Contact.Mail(mail, Contact.MailCategory.PRO);
            _contact.addMail(_mail);
            return _contact;
        };

        this.createContactWithProfessionalMobile = function(gender, firstname, lastname, num) {
            var _contact = new Contact.Contact(gender, firstname, lastname);
            var _phone = new Contact.Phone(
                num,
                Contact.PhoneCategory.PRO,
                Contact.PhoneType.MOBILE
            );
            _contact.addPhone(_phone);
            return _contact;
        };

        this.createContactWithTag = function (gender, firstname, lastname, tag){
            var _contact = new Contact.Contact(gender, firstname, lastname);
            _contact.setTag(tag);
            return _contact;
        };
    };

    return self;
}(Contact || {}));